# Documentation general analysis - Pidgin

## Language/Tone
The website is simple and uses mostly simplified English.
Lots of languages translated to... so this is good.

## Get Started Guide
https://developer.pidgin.im/wiki/Using%20Pidgin#GettingStarted
- Use - get started

## Sections:
- Overview
- Install
- Download
- About - goes to page then categories...it's okay.
  - Community
  - Philosophy
- Help

Interesting strategy.

Good categorization

Simple language

## Help - IRC
Note: IRC section answers a lot of irc questions.

The IRC link is directly below but what if we included a link that opens the IRC client.

Example:

Test your irc by coming to our channel...

This could be good because -
A. They land in your space while using your product
B. They know where to go and where they can go to connect
... question - does the software have the channel to pidgin set by default or no

## Development
This is what we will test> Building pidgin

Just the basics:

- Go to the website
- Find the download
- Install instructions
- Build

While the user can easily install with apt install - the need here is to test the build docs for devs... that is the scope

## Reword some of this.
### Original

If you’re building for Windows you’ll want to have a look at windows instead.

### Reword recommendation
If you are building for Windows, you will want to have a look at <a href="https://pidgin.im/development/building/2.x.y/windows/">Windows instructions</a> instead.

### Dependencies
Pidgin’s default build has a lot of dependencies. This document will hopefully help you find and install them all.

MW: I need to find them all.. it just starts out like - Oh boy... buckle up cupcake...

### Packaged Dependencies
These dependencies are ones that should be found in your distribution’s package manager.

### I have the source; how do I build this thing?
Assuming you have all of the necessary libraries and their headers installed (see the next few questions), you compile libpurple, Pidgin and Finch just like most applications:

$ tar xjvf pidgin-2.x.y.tar.bz2

$ cd pidgin-2.x.y

$ ./configure && make && sudo make install

MW: awesome with the copy button - very good. Command prompts show but do not get copied
Also good with the sudo. Surprisingly even texinfo does not inform users to use sudo/root

==================== Stopping Point ==========================

This will install libpurple, Pidgin and Finch to /usr/local. If you want to install it elsewhere, pass --prefix=/some/other/prefix to ./configure. (You really don’t want to install it to /usr.) See ./configure --help for other options you can change at compile-time.

If you got the source tree from our Mercurial database (which you probably shouldn’t have), you’ll need to run ./autogen.sh instead of ./configure the first time around. If you get an error like the following, you may need a newer version of automake.

running /usr/bin/automake -a -c --gnu... failed.
Makefile.am:79: directory should not contain `/'
pidgin/pixmaps/Makefile.am:4: directory should not contain `/'
If you are trying to compile on Windows, you need the answer to a different question.

Why can’t I compile Pidgin?
You’re probably missing some dependencies. The configure script will tell you when you are missing required dependencies. Remember that if you’re using an RPM-based (RedHat Enterprise Linux, CentOS, SUSE, Mandriva, etc.) or Debian-based system (Debian, Ubuntu, etc.) that having just a library’s package installed is not sufficient–you must also have the -devel (RPM systems) or -dev (Debian-based systems) package for each library installed. If configure is succeeding, but make fails, this is harder to diagnose and you will probably want to drop by the IRC channel or XMPP conference listed on Community to get help.

How do I install the dependencies on Debian or Ubuntu?
You need to install the development headers; these are the -dev packages. A simple apt-get build-dep pidgin will find and install all of the required header packages for you.

If apt-get build-dep fails with a message like

E: You must put some 'source' URIs in your sources.list
then you need to add deb-src lines to your /etc/apt/sources.list corresponding to each of the deb lines already there. If editing configuration files scares you, Ubuntu has a “Software Sources” control panel in System -> Administration which has some magic checkboxes to do this for you.

How do I install the dependencies on Fedora (or similar)?
RPM-based distribution users may find yum-builddep pidgin useful if a source RPM is available and the distribution uses the yum tools.

Note that on current Fedora, you would use dnf builddep pidgin, which is in the dnf-plugins-core package. If you’re still using yum, the yum-builddep command is in the yum-utils package, which is not necessarily installed by default.

How do I apply the patch “something.diff”?
Type patch -p1 < something.diff from the top level of the source directory (pidgin/, not pidgin/pidgin/ or pidgin/finch/).

Is there a way to compile without some protocols?
There are actually two ways:

Run ./configure with the --with-static-prpls option with the --disable-plugins option. This will let you choose which protocols to include by specifying them as a comma-separated list, such as the following (but note that you won’t be able to use any other protocols or plugins): ./configure --disable-plugins --with-static-prpls=irc,bonjour
Use the --with-dynamic-prpls option to ./configure by specifying a comma-separated list, like so: ./configure --with-dynamic-prpls=irc,xmpp


## MW: Future participant opportunities
Later comes testing using it... with a get started or whatever docs


## Blog



## Missing Copyright/copyleft/trademark

## Store
for example... love the store

However, i want stickers....

can i make some?
Need some guidance with trademark
What Other projects are doing (resources)

## Needs work
https://pidgin.im/development/building/2.x.y/#how-do-i-install-the-dependencies-on-debian-or-ubuntu

<img src="copythisno.png">

This allows users to copy an error. Do you have another way to highlight something as code but not with the highlighter or copy code option?
